package com.martynas.student_sorter.service;

import com.martynas.student_sorter.configs.FileStorageProps;
import com.martynas.student_sorter.entities.Student;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileServiceTest {

    FileService fileService;
    FileStorageProps fileStorageProps;

    @Before
    public void setUp() {
        this.fileStorageProps = setUpFileStorageProps();
        this.fileService = new FileService(this.fileStorageProps);

    }

    @Test
    public void uploadFileIntegrationTest() throws IOException {

        String fileName = "myTestFile.txt";

        MockMultipartFile correctFile = new MockMultipartFile("file",
                fileName,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                "test file contents".getBytes());

        fileService.uploadFile(correctFile);

        final String s = new String(Files.readAllBytes(Paths.get(fileStorageProps.getUploadDir() + fileName)));
        Assert.assertEquals("test file contents", s);

        Files.deleteIfExists(Paths.get(this.fileStorageProps.getUploadDir() + fileName));
    }

    @Test
    public void readFileIntegrationTest() throws IOException {

        String readFileName = "readFileTest.txt";

        List<String> values = fileService.readFile(readFileName, fileStorageProps.getUploadDir());
        Assert.assertEquals("Student1,4.3", values.get(0));

    }

    @Test
    public void loadFileArResourceIntegartionTest() throws Exception {

        String fileName = "students_sorted_test.txt";
        String downloadStorage = "src/test/resources/storage/downloads_test/";

        Path filePath = Paths.get(downloadStorage).toAbsolutePath().normalize()
                .resolve(fileName).normalize();
        Resource expectedResource = new UrlResource(filePath.toUri());

        Resource actualResource = fileService.loadFileAsResource(fileName);

        Assert.assertEquals(expectedResource, actualResource);
    }

    @Test
    public void writeToFileIntegrationTest() throws IOException {

        String fileName = "writeToFileTest.txt";
        String sortedFileName = "writeToFileTest_sorted.txt";
        String expectedString = "Student3,9.7\nStudent1,5.9\nStudent2,7.5\n";
        List<Student> students = getStudentsList();

        fileService.writeToFile(fileName, students);

        final String actualContent = new String(
                Files.readAllBytes(Paths.get(fileStorageProps.getDownloadDir() + sortedFileName)));
        Assert.assertEquals(expectedString, actualContent);

        Files.deleteIfExists(Paths.get(this.fileStorageProps.getDownloadDir() + sortedFileName));
    }

    private FileStorageProps setUpFileStorageProps() {
        FileStorageProps fileStorageProps = new FileStorageProps();
        fileStorageProps.setDownloadDir("src/test/resources/storage/downloads_test/");
        fileStorageProps.setUploadDir("src/test/resources/storage/uploads_test/");
        return fileStorageProps;
    }

    private List<Student> getStudentsList() {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student("Student3", 9.7));
        studentList.add(new Student("Student1", 5.9));
        studentList.add(new Student("Student2", 7.5));
        return studentList;
    }
}
