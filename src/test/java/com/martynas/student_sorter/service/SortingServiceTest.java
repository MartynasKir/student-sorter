package com.martynas.student_sorter.service;

import com.martynas.student_sorter.entities.Student;
import com.martynas.student_sorter.utils.sorters.BubbleSort;
import com.martynas.student_sorter.utils.sorters.HeapSort;
import com.martynas.student_sorter.utils.sorters.MergeSort;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SortingServiceTest {

    List<Student> studentsList;

    @Before
    public void setUp() {
        this.studentsList = getStudentsList();
    }

    @Test
    public void bubbleSortTest() {
        BubbleSort.sort(this.studentsList);
        Assert.assertEquals("Student1", this.studentsList.get(0).getName());
        Assert.assertEquals("Student2", this.studentsList.get(1).getName());
        Assert.assertEquals("Student3", this.studentsList.get(2).getName());
    }

    @Test
    public void heapSortTest() {
        HeapSort.sort(this.studentsList);
        Assert.assertEquals("Student1", this.studentsList.get(0).getName());
        Assert.assertEquals("Student2", this.studentsList.get(1).getName());
        Assert.assertEquals("Student3", this.studentsList.get(2).getName());
    }

    @Test
    public void mergeSortTest() {
        int lastListElement = this.studentsList.size() - 1;
        MergeSort.sort(this.studentsList, 0, lastListElement);
        Assert.assertEquals("Student1", this.studentsList.get(0).getName());
        Assert.assertEquals("Student2", this.studentsList.get(1).getName());
        Assert.assertEquals("Student3", this.studentsList.get(2).getName());
    }

    private List<Student> getStudentsList() {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student("Student3", 9.7));
        studentList.add(new Student("Student1", 5.9));
        studentList.add(new Student("Student2", 7.5));
        return studentList;
    }
}
