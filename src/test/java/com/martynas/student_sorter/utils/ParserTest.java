package com.martynas.student_sorter.utils;

import com.martynas.student_sorter.entities.Student;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParserTest {

    List<String> stringValuesList;

    @Before
    public void setUp() {
        this.stringValuesList = getStringValuesList();
    }

    @Test
    public void parserTest() {
        List<Student> students = Parser.getStudents(this.stringValuesList);

        Assert.assertEquals("Student1", students.get(0).getName());
        Assert.assertEquals("Student2", students.get(1).getName());
        Assert.assertEquals("Student3", students.get(2).getName());
        Assert.assertEquals(9.7, students.get(0).getScore(), 0.0001);
        Assert.assertEquals(6.8, students.get(1).getScore(), 0.0001);
        Assert.assertEquals(8.5, students.get(2).getScore(), 0.0001);
    }

    private List<String> getStringValuesList() {
        List<String> valuesList = new ArrayList<>();
        valuesList.add("Student1,9.7");
        valuesList.add("Student2,6.8");
        valuesList.add("Student3,8.5");
        return valuesList;
    }
}
