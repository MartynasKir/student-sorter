package com.martynas.student_sorter.utils.time;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTimeConverterTest {

    @Test
    public void whenNanosecondsGiven_ThenReturnSeconds() {
        long nanoSeconds = 10028900;
        String result = MyTimeConverter.convertNanosecondsToSeconds(nanoSeconds);

        String expectedOutput = ",0100289";
        assertEquals(expectedOutput, result);
    }
}
