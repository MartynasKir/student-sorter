package com.martynas.student_sorter.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.martynas.student_sorter.configs.FileStorageProps;
import com.martynas.student_sorter.service.FileService;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

// ExtendWith only for Junit5 Jupyter
@ExtendWith(SpringExtension.class)
// WebMvcTest load context with beans only needed to test controller
@WebMvcTest(controllers = FileController.class)
class FileControllerTest {

    @MockBean
    HttpServletRequest request;
    @Autowired
    private MockMvc mockMvc;
    //@MockBean automatically replaces the bean of the same type in the application context with a Mockito mock.
    @MockBean
    private FileStorageProps fileStorageProps;
    @MockBean
    private FileService fileService;

    @Test
    public void whenGetInvokedOnIndexMethod_returnViewTemplate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("view"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    void whenCorrectFileUploaded_performRedirect() throws Exception {
        MockMultipartFile correctFile = new MockMultipartFile("file", "myFile.txt", MediaType.MULTIPART_FORM_DATA_VALUE,
                "test".getBytes());
        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/upload")
                .file(correctFile)
                .param("sortmethod", "bubble"))
                .andExpect(MockMvcResultMatchers.status().is(302));

    }

    @Test
    void whenParamsNotProvided_returnBadRequest() throws Exception {
        MockMultipartFile incorrectFile = new MockMultipartFile("file_invalid", "myFile.txt",
                MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());
        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/upload")
                .file(incorrectFile)
                .param("sortmethod_invalid", "bubble"))
                .andExpect(status().is(400));
    }

    @Test
    public void whenCorrectParametersPassed_returnResultsTemplate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/results/sortingmethod/bubble/students_test.txt"))
                .andExpect(status().isOk())
                .andExpect(view().name("results"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void whenCorrectParametersPassed_downloadFile() throws Exception {

        String downloadStorage = "src/test/resources/storage/downloads_test/";
        String fileName = "students_sorted_test.txt";
        Path filePath = Paths.get(downloadStorage).toAbsolutePath().normalize()
                .resolve(fileName).normalize();
        Resource resource = new UrlResource(filePath.toUri());

        Mockito.when(fileService.loadFileAsResource(fileName)).thenReturn(resource);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/download/" + fileName, request)
                .contentType(MediaType.TEXT_PLAIN)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\""))
                .andExpect(status().isOk())
                .andReturn();
    }
}
