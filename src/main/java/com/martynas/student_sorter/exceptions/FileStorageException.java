package com.martynas.student_sorter.exceptions;

public class FileStorageException extends RuntimeException {

    //A class for any exception during file upload process
    private static final long serialVersionUID = 1L;
    private String msg;

    public FileStorageException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
