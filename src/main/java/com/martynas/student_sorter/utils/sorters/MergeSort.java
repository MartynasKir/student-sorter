package com.martynas.student_sorter.utils.sorters;

import com.martynas.student_sorter.entities.Student;
import java.util.ArrayList;
import java.util.List;

public class MergeSort {

    public static void sort(List<Student> students, int l, int r) {
        if (l < r) {
            // Find the middle point
            int m = (l + r) / 2;

            // Sort first and second halves
            sort(students, l, m);
            sort(students, m + 1, r);

            // Merge the sorted halves
            merge(students, l, m, r);
        }

    }

    // Merges two subarrays of list.
    // First sublist is {l..m}
    // Second sublist is {m+1..r}
    private static void merge(List<Student> students, int l, int m, int r) {
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;

        /* Create temp lists */
        List<Student> L = new ArrayList<>();
        List<Student> R = new ArrayList<>();

        /*Copy data to temp arrays*/
        for (int i = 0; i < n1; ++i) {
            L.add(i, students.get(l + i));
        }
        for (int j = 0; j < n2; ++j) {
            R.add(j, students.get(m + 1 + j));
        }

        /* Merge the temp arrays */
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;

        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2) {
            if (L.get(i).getScore() <= R.get(j).getScore()) {
                students.set(k, L.get(i));
                i++;
            } else {
                students.set(k, R.get(j));
                j++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (i < n1) {
            students.set(k, L.get(i));
            i++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (j < n2) {
            students.set(k, R.get(j));
            j++;
            k++;
        }
    }
}
