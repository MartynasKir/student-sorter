package com.martynas.student_sorter.utils.sorters;

import com.martynas.student_sorter.entities.Student;
import java.util.List;

public class BubbleSort {

    public static void sort(List<Student> students) {

        int n = students.size();
        Student temp;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (students.get(j - 1).getScore() > students.get(j).getScore()) {
                    //swap elements
                    temp = students.get(j - 1);
                    students.set(j - 1, students.get(j));
                    students.set(j, temp);
                }
            }
        }
    }
}
