package com.martynas.student_sorter.utils;

import com.martynas.student_sorter.entities.Student;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    public static List<Student> getStudents(List<String> valueList) {
        List<Student> studentList = new ArrayList<>();

        for (String value : valueList) {
            String[] values = value.split(",");
            var name = values[0];
            var score = Double.valueOf(values[1]);
            studentList.add(new Student(name, score));
        }
        return studentList;
    }
}
