package com.martynas.student_sorter.utils.time;

import java.text.DecimalFormat;

public class MyTimeConverter {

    public static String convertNanosecondsToSeconds(long nanoSeconds) {
        double value = (double) nanoSeconds / (double) 1000000000;
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
        return df.format(value);
    }
}
