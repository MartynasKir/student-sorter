package com.martynas.student_sorter.entities;

import java.io.Serializable;

public class Student implements Serializable {

    String name;
    double score;


    public Student(String name, double score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public double getScore() {
        return score;
    }
}
