package com.martynas.student_sorter.service;

import static org.slf4j.LoggerFactory.getLogger;

import com.martynas.student_sorter.configs.FileStorageProps;
import com.martynas.student_sorter.entities.Student;
import com.martynas.student_sorter.exceptions.FileStorageException;
import com.martynas.student_sorter.exceptions.MyFileNotFoundException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

    private static final Logger LOG = getLogger(FileService.class);

    FileStorageProps fileStorageProps;

    public FileService(FileStorageProps fileStorageProps) {
        this.fileStorageProps = fileStorageProps;
    }

    public void uploadFile(MultipartFile file) {
        try {
            Path copyLocation = Paths
                    .get(fileStorageProps.getUploadDir() + File.separator + StringUtils
                            .cleanPath(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileStorageException(
                    "Could not store file: '" + file.getOriginalFilename() + "'. Please try again later.");
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = Paths.get(fileStorageProps.getDownloadDir()).toAbsolutePath().normalize()
                    .resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException e) {
            throw new MyFileNotFoundException("File not found " + fileName);
        }
    }

    public List<String> readFile(String fileName, String fileLocation) throws IOException {
        List<String> valuesList = new ArrayList<>();
        String filePath = fileLocation + fileName;
        File file = new File(filePath);

        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis, "UTF8");
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                valuesList.add(line);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return valuesList;
    }

    public void writeToFile(String fileName, List<Student> students) throws IOException {

        String newFileName = fileName.replaceFirst("[.][^.]+$", "") + "_sorted.txt";
        LOG.info("New file name is: {}", newFileName);

        String newFilePath = fileStorageProps.getDownloadDir() + newFileName;

        FileOutputStream out = new FileOutputStream(newFilePath);
        for (Student student : students) {
            var str = student.getName() + "," + student.getScore();
            byte[] strToBytes = str.getBytes();
            out.write(strToBytes);
            out.write("\n".getBytes());
        }
        out.close();
    }

    public List<Student> sortFileContent(List<Student> students, String sortingMethod) throws IOException {
        if ("bubble".equals(sortingMethod)) {
            SortingService.boubbleSort(students);
        } else if ("heap".equals(sortingMethod)) {
            SortingService.heapSort(students);
        } else if ("merge".equals(sortingMethod)) {
            SortingService.mergeSort(students);
        } else {
            LOG.info("No such sorting method available: {}", sortingMethod);
        }
        return students;
    }
}
