package com.martynas.student_sorter.service;

import com.martynas.student_sorter.entities.Student;
import com.martynas.student_sorter.utils.sorters.BubbleSort;
import com.martynas.student_sorter.utils.sorters.HeapSort;
import com.martynas.student_sorter.utils.sorters.MergeSort;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class SortingService {

    public static void boubbleSort(List<Student> students) {
        BubbleSort.sort(students);
    }

    public static void heapSort(List<Student> students) {
        HeapSort.sort(students);
    }

    public static void mergeSort(List<Student> students) {
        MergeSort.sort(students, 0, students.size() - 1);
    }

}
