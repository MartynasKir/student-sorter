package com.martynas.student_sorter.controllers;

import static org.slf4j.LoggerFactory.getLogger;

import com.martynas.student_sorter.configs.FileStorageProps;
import com.martynas.student_sorter.entities.Student;
import com.martynas.student_sorter.service.FileService;
import com.martynas.student_sorter.utils.Parser;
import com.martynas.student_sorter.utils.time.MyTimeConverter;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class FileController {

    private static final Logger LOG = getLogger(FileController.class);

    private final FileStorageProps fileStorageProps;
    private final FileService fileService;

    public FileController(FileStorageProps fileStorageProps, FileService fileService){
        this.fileStorageProps = fileStorageProps;
        this.fileService = fileService;
    }

    @GetMapping("/")
    public String index(Model model) {
        return "view";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file,
            @RequestParam("sortmethod") String sortingMethod,
            RedirectAttributes redirectAttributes) throws IOException {

        LOG.info("sortingMethod is: {}", sortingMethod);
        LOG.info("file original name: {}", file.getOriginalFilename());
        LOG.info("file name: {}", file.getName());
        LOG.info("file size: {} bytes", file.getSize());

        fileService.uploadFile(file);

        redirectAttributes.addFlashAttribute("message",
                "File successfully uploaded: '" + file.getOriginalFilename() + "'.");
        redirectAttributes.addFlashAttribute("resultsUrl",
                "results/sortingmethod/" + sortingMethod + "/" + file.getOriginalFilename());
        return "redirect:/";
    }

    @GetMapping("/results/sortingmethod/{sortingMethod}/{fileName:.+}")
    public String results(Model model,
            @PathVariable String sortingMethod,
            @PathVariable String fileName) throws IOException {

        //readFile
        List<String> values = fileService.readFile(fileName, fileStorageProps.getUploadDir());
        //parse Student data from string lines and build student list
        List<Student> students = Parser.getStudents(values);

        long startTime = System.nanoTime();
        List<Student> studentsSorted = fileService.sortFileContent(students, sortingMethod);
        long stopTime = System.nanoTime();
        String sortingTime = MyTimeConverter.convertNanosecondsToSeconds(stopTime - startTime);

        fileService.writeToFile(fileName, studentsSorted);

        String sortedFileName = fileName.replaceFirst("[.][^.]+$", "") + "_sorted.txt";

        model.addAttribute("students", studentsSorted);
        model.addAttribute("downloadUrl", "/download/" + sortedFileName);
        model.addAttribute("sortingInfo", studentsSorted.size() +
                " results sorted in " + sortingTime + " seconds");
        return "results";
    }

    @GetMapping("/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {

        Resource resource = fileService.loadFileAsResource(fileName);

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            LOG.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "text/plain";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
